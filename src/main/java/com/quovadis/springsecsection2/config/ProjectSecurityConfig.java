package com.quovadis.springsecsection2.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

@Configuration
public class ProjectSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception{

        /**
         * Default configurations which vill secure all the requests
         */

        /*http.authorizeRequests((requests) -> {
            ((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)requests.anyRequest()).authenticated();
        });
        http.formLogin();
        http.httpBasic();*/

        /**
         * Custom configurations as per our requirement
         */
        /*http
                .authorizeRequests()
                    .antMatchers("/myAccount").authenticated()
                    .antMatchers("/myBalance").authenticated()
                    .antMatchers("/myLoans").authenticated()
                    .antMatchers("/myCards").authenticated()
                    .antMatchers("/notices").permitAll()
                    .antMatchers("/contact").permitAll()
                    .and()
                .formLogin().and()
                .httpBasic();*/

        /**
         * Configurations to deny all the requests
         */

        /*http
                .authorizeRequests()
                    .anyRequest()
                    .denyAll()
                .and()
                .formLogin()
                .and()
                .httpBasic();*/

        /**
         * Configurations to permit all the requests
         */

        http
                .authorizeRequests()
                    .anyRequest()
                    .permitAll()
                .and()
                .formLogin()
                .and()
                .httpBasic();

    }
}
